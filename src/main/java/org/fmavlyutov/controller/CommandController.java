package org.fmavlyutov.controller;

import org.fmavlyutov.api.controller.ICommandController;
import org.fmavlyutov.api.service.ICommandService;
import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;
import org.fmavlyutov.exception.system.CommandNotFoundException;
import org.fmavlyutov.model.Command;
import org.fmavlyutov.util.NumberUtil;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void displayHello() {
        System.out.println("[START PROGRAM]\n");
    }

    @Override
    public void displayGoodbye() {
        System.out.println("[FINISH PROGRAM]");
    }

    @Override
    public void displayHelp() {
        for (final Command command : commandService.getCommands()) {
            System.out.println(command);
        }
        System.out.println();
    }

    @Override
    public void displayVersion() {
        System.out.println("Version: 1.0.0\n");
    }

    @Override
    public void displayAbout() {
        System.out.println("Author: Philip Mavlyutov\n");
    }

    @Override
    public void displayInfo() {
        long availableProccers = Runtime.getRuntime().availableProcessors();
        System.out.printf("Available processors: %d cores\n", availableProccers);
        long totalMemory = Runtime.getRuntime().totalMemory();
        System.out.printf("Total memory: %s\n", NumberUtil.formatBytes(totalMemory));
        long maxMemory = Runtime.getRuntime().maxMemory();
        String maxMemoryFormat = maxMemory == Long.MAX_VALUE ? "no limit" : NumberUtil.formatBytes(maxMemory);
        System.out.printf("Maximum memory: %s\n", maxMemoryFormat);
        long usedMemory = totalMemory - Runtime.getRuntime().freeMemory();
        System.out.printf("Used memory: %s\n", NumberUtil.formatBytes(usedMemory));
        System.out.println();
    }

    @Override
    public void displayArgumentError() {
        System.out.printf("Argument not found. Enter [%s] to see all arguments\n", CommandLineArgument.HELP);
        System.out.println();
    }

    @Override
    public void displayCommands() {
        for (final Command command : commandService.getCommands()) {
            if (command == null) {
                continue;
            }
            String name = command.getName();
            if (name == null || name.isEmpty()) {
                continue;
            }
            System.out.println(name);
        }
        System.out.println();
    }

    @Override
    public void displayArguments() {
        for (final Command command : commandService.getCommands()) {
            if (command == null) {
                continue;
            }
            String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) {
                continue;
            }
            System.out.println(argument);
        }
        System.out.println();
    }

}
