package org.fmavlyutov.controller;

import org.fmavlyutov.api.controller.ITaskController;
import org.fmavlyutov.api.service.ITaskService;
import org.fmavlyutov.enumerated.Sort;
import org.fmavlyutov.enumerated.Status;
import org.fmavlyutov.model.Task;
import org.fmavlyutov.util.TerminalUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.create(name, description);
        showTask(task);
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
    }

    @Override
    public void showTasks() {
        System.out.println("[TASKS LIST]");
        System.out.println("Enter sort:");
        System.out.println(Arrays.toString(Sort.values()));
        final String sort = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAll(Sort.toSort(sort));
        int index = 1;
        for (Task task : tasks) {
            if (task == null) {
                continue;
            }
            System.out.println(index++ + ". " + task.getName());
        }
    }

    @Override
    public void showTaskById() {
        System.out.println("[SHOW TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.findOneById(id);
        showTask(task);
    }

    @Override
    public void showTaskByIndex() {
        System.out.println("[SHOW TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.findOneByIndex(index);
        showTask(task);
    }

    @Override
    public void showTaskByProjectId() {
        System.out.println("[SHOW TASK BY PROJECT ID]");
        System.out.println("Enter project id:");
        final String projectId = TerminalUtil.nextLine();
        final List<Task> tasks = taskService.findAllByProjectId(projectId);
        showTask(tasks);
    }

    @Override
    public void updateTaskById() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateById(id, name, description);
        showTask(task);
    }

    @Override
    public void updateTaskByIndex() {
        System.out.println("[UPDATE TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter name:");
        final String name = TerminalUtil.nextLine();
        System.out.println("Enter description:");
        final String description = TerminalUtil.nextLine();
        final Task task = taskService.updateByIndex(index, name, description);
        showTask(task);
    }

    @Override
    public void removeTaskById() {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        taskService.removeById(id);
    }

    @Override
    public void removeTaskByIndex() {
        System.out.println("[REMOVE TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        taskService.removeByIndex(index);
    }

    @Override
    public void startTaskById() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.COMPLETED);
        showTask(task);
    }

    @Override
    public void startTaskByIndex() {
        System.out.println("[START TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.IN_PROGRESS);
        showTask(task);
    }

    @Override
    public void completeTaskById() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter id:");
        final String id = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.COMPLETED);
        showTask(task);
    }

    @Override
    public void completeTaskByIndex() {
        System.out.println("[COMPLETE TASK]");
        System.out.println("Enter index:");
        final Integer index = TerminalUtil.nextInt() - 1;
        final Task task = taskService.changeStatusByIndex(index, Status.COMPLETED);
        showTask(task);
    }

    @Override
    public void changeTaskStatusById() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("Enter index:");
        Integer index = TerminalUtil.nextInt() - 1;
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusByIndex(index, Status.toStatus(status));
        showTask(task);
    }

    @Override
    public void changeTaskStatusByIndex() {
        System.out.println("[CHANGE TASK STATUS]");
        System.out.println("Enter index:");
        String id = TerminalUtil.nextLine();
        System.out.println("Enter status:");
        System.out.println(Arrays.toString(Status.values()));
        final String status = TerminalUtil.nextLine();
        final Task task = taskService.changeStatusById(id, Status.toStatus(status));
        showTask(task);
    }

    private void showTask(final Task task) {
        if (task == null) {
            return;
        }
        System.out.println(task);
    }

    private void showTask(final Collection<Task> tasks) {
        for (final Task task : tasks) {
            showTask(task);
        }
    }

}
