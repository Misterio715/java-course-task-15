package org.fmavlyutov.controller;

import org.fmavlyutov.api.controller.IProjectTaskConroller;
import org.fmavlyutov.api.service.IProjectTaskService;
import org.fmavlyutov.util.TerminalUtil;

public class ProjectTaskConroller implements IProjectTaskConroller {

    private final IProjectTaskService projectTaskService;

    public ProjectTaskConroller(IProjectTaskService projectTaskService) {
        this.projectTaskService = projectTaskService;
    }

    @Override
    public void bindTaskToProject() {
        System.out.println("[BIND TASK TO PROJECT]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.bindTaskToProject(projectId, taskId);
    }

    @Override
    public void unbindTaskFromProject() {
        System.out.println("[UNBIND TASK TO PROJECT]");
        System.out.println("Enter project id: ");
        final String projectId = TerminalUtil.nextLine();
        System.out.println("Enter task id: ");
        final String taskId = TerminalUtil.nextLine();
        projectTaskService.unbindTaskFromProject(projectId, taskId);
    }

}
