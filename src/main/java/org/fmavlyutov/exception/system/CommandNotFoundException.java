package org.fmavlyutov.exception.system;

public final class CommandNotFoundException extends AbstractSystemException {

    public CommandNotFoundException(String message) {
        super(message);
    }

}
