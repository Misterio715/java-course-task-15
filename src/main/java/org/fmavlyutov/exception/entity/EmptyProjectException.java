package org.fmavlyutov.exception.entity;

public final class EmptyProjectException extends AbstractEntityException {

    public EmptyProjectException() {
        super("Project can not have empty values!");
    }

}
