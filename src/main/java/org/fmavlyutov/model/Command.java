package org.fmavlyutov.model;

import org.fmavlyutov.constant.CommandLineArgument;
import org.fmavlyutov.constant.CommandLineConstant;

public final class Command {

    private String name;

    private String argument;

    private String description;

    public Command(String name, String argument, String description) {
        this.name = name;
        this.argument = argument;
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getArgument() {
        return argument;
    }

    public void setArgument(String argument) {
        this.argument = argument;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        if (name != null && !name.isEmpty()) {
            sb.append(name);
        }
        if (argument != null && !argument.isEmpty()) {
            sb.append(" | ").append(argument);
        }
        sb.append("]");
        if (description != null && !description.isEmpty()) {
            sb.append(" - ").append(description);
        }
        return sb.toString();
    }

}
